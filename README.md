Readme
======

This is a wiki with a collection of notes about porting scientific codes to Power8 (ppc64le) and later derivatives.

This is part of the bounty program located at https://www.bountysource.com/teams/ibm-hpc

Getting Started
===============

You can port on any Power8 system, but the follow lists a few ways you can get access without having your own hardware and at no cost.

Check to see if the package is already in the [wiki](https://bitbucket.org/umarcts/ppc64le-wiki/wiki/)

IBM Power Development Cloud
---------------------------

Create an IBM partnerworld Power Development Cloud (PDP) account and register as an open source developer: http://www.ibm.com/partnerworld/pdp

 * Email account needs to be something other than @gmail, @yahoo, etc.  A university email or corporate will work better.
 * Open source developers require a phone number be attached. PDP will only make you aware of this after you register. It may take a couple hours after adding your phone number for the data to show up on the PDP application form. After you add your phone number to your account and submit the PDP application form, it will take up to a day for your application to be approved.
 * Once your application is approved, you may create and view your programs at: https://www-356.ibm.com/partnerworld/wps/ent/pdp/web/MyProgramAccess
 * Each program grants you access to a system for a specific length of time. You must make a reservation for the system at least two hours in advance, and for up to two weeks. From the My Program Access page, choose "**Make a reservation request**:" and select **Open Source Developer** to open the form. 
	* Select an image: "**RedHat Linux 7.1 LE with IBM xlc/xlf**"
	* If this is not your first reservation, you may load the image of a previously reserved virtual machine by choosing Select an image type: "Saved image".
	* Click "**Add resources to project**" to add a virtual machine with the configured parameters to your reservation.
 * Once your reservation is active, you can view the details of your virtual machine on the My Program Access page, collapsed under Open Source Developer. Follow the instructions at https://public.dhe.ibm.com/partnerworld/pub/pdp/connecting_to_your_pdp_system_a_user_guide.pdf to connect to the specified VPN with Cisco AnyTime Mobility Client and connect to your VM with puTTY or the SSH client of your choice.